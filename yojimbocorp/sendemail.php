<?php
$siteName = "yojimbocorp";

$name = $_POST['name'];
$mail = $_POST['email'];
$subject = $_POST['subject'];
$message = $_POST['message'];

if (isset($name) && isset($mail) && isset($message)) {
	
	
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/plain; charset=iso-8859-1' . "\r\n";

// Additional headers
$headers .= 'To: Customer Service <info@yojimbocorp.com>' . "\r\n";
$headers .= 'From: Website Contact Form <admin@yojimbocorp.com>' . "\r\n";
$headers .= 'Reply-To: '.$mail . "\r\n";

//declare who youre sending to
$to = "info@yojimbocorp.com";

//create a more personal message
$sub = 'Contact Form';
$details = $name.' just submitted a website form from yojimbocorp.com'. "\r\n\r\n";
$details .='Subject: '.$subject. "\r\n\r\n";
$details .='Message: '.$message. "\r\n";
$details .="\r\n".'Reply to this email to respond directly (using the email address they provided). ';
$details .= 'Message Sent On: ['. date('Y-m-d').']. End of message.';
mail($to,$sub,$details, $headers);
	
}else{
	echo '0';
}