<DOCTYPE html>
<html lang="en">
<head>
  <title>VIN Input</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- add jQuery -->
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
  <!-- add bootstrap -->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <!-- user typed js for form -->
  <script src="e2script.js"></script>
</head>
<body>
	
<div class="wrap"> 
<h1 align="center">VIN Input</h1>
<p align="center">Please input a VIN for testing.</p> <br><br>
<div class="col-md-4"><!--quick spacer :)--></div>
	<div class="col-md-5">
		<form name="post_content" action="" method="post">
			<label for="change_record">Enter A VIN:</label>
			<input type="text" class="form-control" name="change_record" placeholder="Change Title"></input><br><br>
			<input type="button" id="record_button" value="Submit" class="btn btn-default"></input>
		</form>
	</div>
</div>
</div>

<div id="VINoutput"></div>
</body>
</html>

