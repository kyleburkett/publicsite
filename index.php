<?php
include 'inc/header.php';
include 'inc/navbar.php';
?>
<section class="home" id="home" data-stellar-background-ratio="0.4">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="st-home-unit">
					<div class="hero-txt">
						<p class="hero-work">Telephony  - Web Development - Advertising</p>
						<h2 class="hero-title">Tech Consulting That Works</h2>
						<a href="#service" class="btn btn-main btn-lg">Find Out More</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about" id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title st-center">
					<h3>Data Operations</h3>
					<p>data driven decision makers</p>
				</div>
				<div class="row mb90">
					<div class="col-md-6">
						<p>Hire someone else and guess all you want, that's at your expense. We make strategic decisions based on data that shows what is working and what's not. This thought process doesn't just apply to advertising, but across the board to all development and systems. How many times have users clicked for more information? None? Lets remove that button and put something more useful there. We also know our back ends from our fronts. We spend time making sure it functions as well as it looks.</p>
					</div>
					<div class="col-md-6">
						<img src="photos/about.jpg" alt="" class="img-responsive">
					</div>
				</div>	
			</div>
		</div>
	</div>
</section>

<section class="funfacts" data-stellar-background-ratio="0.4">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="funfact">
					<div class="st-funfact-icon"><i class="fa fa-briefcase"></i></div>
					<div class="st-funfact-counter" ><span class="st-ff-count" data-from="0" data-to="25" data-runit="1">0</span>+</div>
					<strong class="funfact-title">Projects Completed</strong>
				</div><!-- .funfact -->
			</div>
			<div class="col-md-4">
				<div class="funfact">
					<div class="st-funfact-icon"><i class="fa fa-clock-o"></i></div>
					<div class="st-funfact-counter" ><span class="st-ff-count" data-from="0" data-to="1000" data-runit="1">0</span>+</div>
					<strong class="funfact-title">Hours of Work</strong>
				</div><!-- .funfact -->
			</div>
			<div class="col-md-4">
				<div class="funfact">
					<div class="st-funfact-icon"><i class="fa fa-send"></i></div>
					<div class="st-funfact-counter" ><span class="st-ff-count" data-from="0" data-to="75" data-runit="1">0</span>+</div>
					<strong class="funfact-title">Feature Changes</strong>
				</div><!-- .funfact -->
			</div>			
		</div>
	</div>
</section>

<section class="service" id="service">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title st-center">
					<h3>What we do</h3>
					<p>Making Business Easier</p>
				</div>
				<div class="row">
					<div class="col-md-3">
						<div class="st-feature">
							<div class="st-feature-icon"><i class="fa fa-mobile"></i></div>
							<strong class="st-feature-title">Mobile vs Cloud</strong>
							<p>Assign a distinct business number to your mobile, or answer the call from your computer.</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="st-feature">
							<div class="st-feature-icon"><i class="fa fa-code"></i></div>
							<strong class="st-feature-title">Web Development</strong>
							<p>Have an ugly website, or none at all? We make some beautiful, functional, personalized sites.</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="st-feature">
							<div class="st-feature-icon"><i class="fa fa-google"></i></div>
							<strong class="st-feature-title">Advertising</strong>
							<p>Experts in managing, creating, and distributing the right advertising for your business.</p>
						</div>
					</div>
					<div class="col-md-3">
						<div class="st-feature">
							<div class="st-feature-icon"><i class="fa fa-bar-chart-o"></i></div>
							<strong class="st-feature-title">Data and Tracking</strong>
							<p>Knowing that something isn't working is the most important piece of the puzzle, in everything.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!--<section class="features-desc">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img src="photos/feature.png" alt="" class="img-responsive">
			</div>
			<div class="col-md-7">
				<h3 class="bottom-line">MULTIPLE DEVICES? WE FEEL YOU.</h3>
				<p>Having multiple devices is the new thing. Phone, laptop, tablet, or walkie talkie. Ok, maybe not that last one. Doesn't matter. What does matter is that you can work on it all, and your website should too... and your calls to clients should too. </p>
			</div>
		</div>
	</div>
</section>-->

<section class="call-2-acction" data-stellar-background-ratio="0.4">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="c2a">
					<h2>MULTIPLE DEVICES? WE FEEL YOU.</h2>
					<p>Having multiple devices is the new thing. Phone, laptop, tablet, or walkie talkie. Ok, maybe not that last one. Doesn't matter. What does matter is that you can work on it all, and your website should too.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<!--
<section class="portfolio" id="portfolio">
	<div class="container-fluid ">
		<div class="row">
			<div class="col-md-12 no-padding ">
				<div class="section-title st-center">
					<h3>What we have done</h3>
					<p>Avocent deditum long</p>
				</div>
				<div class="filter mb40">
					<form id="filter">
					<fieldset class="group">
						<label class="btn btn-default btn-main"><input type="radio" name="filter" value="all" checked="checked">All</label>
						<label class="btn btn-default"><input type="radio" name="filter" value="photography">Photography</label>
						<label class="btn btn-default"><input type="radio" name="filter" value="design">Design</label>
						<label class="btn btn-default"><input type="radio" name="filter" value="codding">Codding</label>
					</fieldset>
					</form><!-- #filter --
				</div><!-- .filter .mb40 --

				<div class="grid">
					<figure class="portfolio-item" data-groups='["photography"]'>
						<img src="photos/portfolio.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["design"]'>
						<img src="photos/portfolio2.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["photography"]'>
						<img src="photos/portfolio3.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["design"]'>
						<img src="photos/portfolio4.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["design"]'>
						<img src="photos/portfolio5.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["photography"]'>
						<img src="photos/portfolio6.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["codding"]'>
						<img src="photos/portfolio7.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["photography"]'>
						<img src="photos/portfolio8.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["codding"]'>
						<img src="photos/portfolio9.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["codding"]'>
						<img src="photos/portfolio10.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["design"]'>
						<img src="photos/portfolio11.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
					<figure class="portfolio-item" data-groups='["design"]'>
						<img src="photos/portfolio12.jpg" alt=""/>
						<figcaption>
							<h2>Nice <span>Lily</span></h2>
							<p>Lily likes to play with crayons and pencils</p>
							<a href="#" class="btn btn-main"><i class="fa fa-link"></i> View more</a>
						</figcaption>
					</figure>
				</div>

			</div>
		</div>
	</div>
</section>-->
<!-- I WANT THE FOLLOWING SECTION, JUST NEED THE INFORMATIONS:
<section class="clients">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- <div class="section-title st-center">
					<h3>Some Of our clients</h3>
					<p>Avocent deditum long</p>
				</div> --
				<ul class="clients-carousel">
					<li><img src="photos/client.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client2.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client3.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client4.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client5.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client6.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client7.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client8.png" class="img-responsive" alt=""></li>
					<li><img src="photos/client9.png" class="img-responsive" alt=""></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="testimonials">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="testimonials-carousel">
					<ul>
						<li>
							<div class="testimonial">
								<div class="testimonial-img">
									<img src="photos/client.jpg" alt="">
								</div>
								<blockquote>
									<p>Tueri tantis inter variis deterritum facta caret pleniorem, efficiat affert quiete, commodis comparat facio ponti, adolescens recta iucundius mundi nostrum viris quae utilitatibus.</p>
									<footer>Joseph Thompson, <cite title="Source Title">Example Inc.</cite></footer>
								</blockquote>
							</div>
						</li>
						<li>
							<div class="testimonial">
								<div class="testimonial-img">
									<img src="photos/client2.jpg" alt="">
								</div>
								<blockquote>
									<p>Contrariis labore vetuit scaevola, contra percurri adamare efficeret quibus. Nostram consulatu mediocritatem maiorem, cyrenaicisque, quandam accedit veniat cognitioque, animadvertat accusantibus temporibus maximeque litterae.</p>
									<footer>Nancy Ford, <cite title="Source Title">Example Inc.</cite></footer>
								</blockquote>
							</div>
						</li>
						<li>
							<div class="testimonial">
								<div class="testimonial-img">
									<img src="photos/client3.jpg" alt="">
								</div>
								<blockquote>
									<p>Illas, volumus prosperum. Nostras eoque statua cuius corrumpit praetor aliter quaeso propter ei, quam inducitur ruant doctiores sanguinem atomum molestiae, antiqua inculta dicent.</p>
									<footer>Arthur Fernandez, <cite title="Source Title">Example Inc.</cite></footer>
								</blockquote>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>-->

<section class="pricing" id="pricing">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title st-center">
					<h3>Our Packages</h3>
					<p>Prices Starting At</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<div class="pricing-table">
					<div class="pricing-header">
						<div class="pt-price">$99.99<small>/m</small></div>
						<div class="pt-name">Telephony</div>
					</div>
					<div class="pricing-body">
						<ul>
							<li><i class="fa fa-check"></i> Call Routing</li>
							<li><i class="fa fa-check"></i> Web Interface</li>
							<li><i class="fa fa-check"></i> 1-10 Users</li>
							<li><i class="fa fa-check"></i> 5k Minutes</li>
						</ul>
					</div>
					<div class="pricing-footer">
						<a href="#contact" class="btn btn-default">Purchase</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pricing-table">
					<div class="pricing-header">
						<div class="pt-price">$25.00<small>/h</small></div>
						<div class="pt-name">Development</div>
					</div>
					<div class="pricing-body">
						<ul>
							<li><i class="fa fa-check"></i> Responsive</li>
							<li><i class="fa fa-check"></i> Custom Features</li>
							<li><i class="fa fa-check"></i> Custom Design</li>
							<li><i class="fa fa-check"></i> User Tracking</li>
						</ul>
					</div>
					<div class="pricing-footer">
						<a href="#contact" class="btn btn-default">Purchase</a>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="pricing-table"><!-- featured">-->
					<div class="pricing-header">
						<div class="pt-price">$69.99<small>/m</small></div>
						<div class="pt-name">Advertising</div>
						<!--<div class="featured-text">Best Value</div>-->
					</div>
					<div class="pricing-body">
						<ul>
							<li><i class="fa fa-check"></i> Business Listings</li>
							<li><i class="fa fa-check"></i> SEO Testing</li>
							<li><i class="fa fa-check"></i> Ad Testing</li>
							<li><i class="fa fa-check"></i> Monthly Reports</li>
						</ul>
					</div>
					<div class="pricing-footer">
						<a href="#contact" class="btn btn-default">Purchase</a>
					</div>
				</div>
			</div>			
		</div>
	</div>
</section>
<section class="faq-sec" id="faq-sec">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- <h2 class="tac">frequently asked questions</h2> -->
				<div class="section-title st-center">
					<h3>Customer Inspired</h3>
					<p>frequently asked questions</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="faq">
					<h3><i class="fa fa-question-circle"></i> Is there a contract?</h3>
					<p>There is always a contract, but mostly to protect you as the client. Contract length depends mostly on the service you are purchasing. Something like development won't have a term- but something like hosting will have a year-long contract. </p>
				</div>
				<div class="faq">
					<h3><i class="fa fa-question-circle"></i> Do you hit deadlines?</h3>
					<p>We have a method for setting milestones, accepting feedback, and hitting deadlines. Depending on the client, deadlines are either easy to meet or hard to meet- but we do our best to define work from day one and schedule it appropriately.</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="faq">
					<h3><i class="fa fa-question-circle"></i> Why are you better?</h3>
					<p> Having a professional's advice always helps to make things run a lot smoother. Whether you are just starting a business, or have an existing company you're working with, give us a call. We love to be transparent. </p>
				</div>
				<div class="faq">
					<h3><i class="fa fa-question-circle"></i> What about outages?</h3>
					<p>Most everything we do is hosted on AWS servers where we've never had an issue with uptime. Also, we put uptime guarantees in the contract so you never have to worry about paying for a service you arent recieving.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="call-us">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3>If you like to work with us</h3>
				<a href="tel:3013024592" class="btn btn-default-o btn-lg">Call Us Now</a>
			</div>
		</div>
	</div>
</section>
<!--
<section class="subscribe">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="subscribe-title">Subscribe Newsletter</h3>
				<form  role="form" class="subscribe-form">
					<div class="input-group">
						<input type="email" class="form-control" id="mc-email" placeholder="Enter E-mail...">
						<span class="input-group-btn">
							<button class="btn btn-main btn-lg" type="submit">Subscribe!</button>
						</span>
					</div>
				</form>
				<div class="subscribe-result"></div>
				<p class="subscribe-or">or</p>
				<ul class="subscribe-social">
					<li><a href="#" class="social twitter"><i class="fa fa-twitter"></i> Follow</a></li>
					<li><a href="#" class="social facebook"><i class="fa fa-facebook"></i> Like</a></li>
					<li><a href="#" class="social rss"><i class="fa fa-rss"></i> RSS</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>
-->
<section class="contact" id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="section-title st-center">
					<h3>Contact Us</h3>
					<p>Get in Touch with Us</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<form class="contact-form" role="form">
					<input type="text" class="form-control" id="fname" name="fname" placeholder="Your Full Name">
					<input type="email" class="form-control" id="email" name="email" placeholder="Your E-mail">
					<input type="text" class="form-control" id="subj" name="subj" placeholder="Your Subject">
					<textarea id="mssg" name="mssg" placeholder="Your Message" class="form-control" rows="10"></textarea>
					<button class="btn btn-main btn-lg" type="submit" id="send" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Sending..."><i class="fa fa-paper-plane "></i> Send</button>
				</form>
				<div id="result-message" role="alert"></div>
			</div>
			<div class="col-md-6">
				<p>Whether you are just curious about starting a business, or you've been working with another agency and are looking for some next steps- reach out to us. We are more than happy to talk quickly about your needs and even give some free advice. Let us help you take that next step you are looking for. Your success is our success.</p>
				<address>
					<strong>DatOps, Information Technology Services</strong><br>
					Greenville, SC 29609<br>
					<abbr title="Phone">P:</abbr> (301) 302-4592 <br>
					<abbr title="Email">E:</abbr> info@greenvilleservices.com <br>
				</address>
				<h2 class="blend">Greenville Information Tech Company</h2>
				<strong class="blend">Located in Greenville South Carolina, We are a Technology company focused on tech in Greenville and Greenville only. Tech can be confusing, but as consultants, we help... by consulting. We consult on things like SEO, web design, marketing, strategy, consulting, greenville, technology, key words, search helpers, etc.</strong>
			</div>
		</div>
	</div>
</section>
<?php include 'inc/footer.php'; ?>		
