<?php
/**
 * Created by PhpStorm.
 * User: kaburke
 * Date: 5/1/2017
 * Time: 12:49 AM
 */
$title = $_POST["title"];
$description = $_POST["description"];
$image_content = $_POST["image_content"];
$image_url = $_POST["image_url"];
$file_content = construct($title, $description, $image_content, $image_url);
$file_name = write_to_file($file_content);
if (is_null($file_name)){
    http_response_code(500);
}
else{
    echo "http://$_SERVER[HTTP_HOST]/linkedin-share-tool/links/".$file_name;
}
function write_to_file($file_content){
    date_default_timezone_set('UTC');
    $filename = strtotime("now").".html";
    $myFile = "./links/".$filename;
    $fh = fopen($myFile, 'w', false); // or die("error");
    $stringData = $file_content;
    fwrite($fh, '');
    fwrite($fh, $stringData);
    fclose($fh);
    return $filename;
}

function construct($title, $description, $image_content, $image_url){
    $html = '<html prefix="og: http://ogp.me/ns#"><head><meta property="og:title" content="'.$title.'" /><meta property="og:description" content="'.$description.'" /> <meta property="og:url" content="'.$image_content.'"/> <meta property="og:image" content="'.$image_url.'" /> </head><body></body><script> window.location = "'.$image_content.'";</script></html> ';
    return $html;
}
?>

